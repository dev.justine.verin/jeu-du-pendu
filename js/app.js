const aAccents = "àâä";
const eAccents = "éèêë";
const iAccents = "ìïî";
const oAccents = "òôö";
const uAccents = "ùûü";
const lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
let count = 1;
let nombreLettres = 0;
let imagePendu = document.getElementById("image-pendu");
let zoneMot = document.getElementById("zone-mot-pendu");
let zoneClavier = document.getElementById("clavier-pendu");
let listeTouches = zoneClavier.children;
let listeEspaces = zoneMot.children;
let motATrouver = "";
let nombreErreurs = document.getElementById("nbre-erreurs");
let boutonNouvellePartie = document.getElementById("bouton-nouvelle-partie");
let resultat = document.getElementById("resultat");
function removeAccent(motATrouverElement) {
  let motATrouverEltMin = motATrouverElement.toLowerCase();
  if (aAccents.includes(motATrouverEltMin)) {
    return "a";
  }
  if (eAccents.includes(motATrouverEltMin)) {
    return "e";
  }
  if (iAccents.includes(motATrouverEltMin)) {
    return "i";
  }
  if (oAccents.includes(motATrouverEltMin)) {
    return "o";
  }
  if (uAccents.includes(motATrouverEltMin)) {
    return "u";
  }
  return motATrouverEltMin;
}

function sontLettresEgales(motATrouverElement, lettreTouche) {
  let lettreDuMot = removeAccent(motATrouverElement);
  let lettreToucheMin = lettreTouche.toLowerCase();
  return lettreDuMot === lettreToucheMin;
}

function afficherLettre(i, motATrouver) {
  listeEspaces.item(i).textContent = motATrouver[i];
}

async function gagnerPartie() {
  resultat.textContent = "Vous avez gagné!"
  for (let childNode of zoneClavier.childNodes){
    childNode.disabled = true;
}}

async function verifierLettre(motATrouver, lettreTouche) {
  let estLettreTrouvee = false;
  for (let i = 0; i < motATrouver.length; i++) {
    if (sontLettresEgales(motATrouver[i], lettreTouche)) {
      afficherLettre(i, motATrouver);
      estLettreTrouvee = true;
      nombreLettres--;
    }
  }
  if (nombreLettres === 0) {
    await gagnerPartie();
  }
  if (!estLettreTrouvee) {
    await avancerJeuMauvaiseReponse();
  }
}

function insererToucheClavier(lettre) {
  let toucheClavier = document.createElement("button");
  toucheClavier.textContent = lettre;
  toucheClavier.className = "touche-clavier";
  zoneClavier.append(toucheClavier);
}

function afficherClavier() {
  for (let lettre of lettres) {
    insererToucheClavier(lettre);
  }
}

async function choisirMot() {
  let list1JsonResponse = await fetch("../json/list1.json");
  let list1 = await list1JsonResponse.json();
  let randomNumber = Math.floor(Math.random() * list1.list.length);
  motATrouver = list1.list[randomNumber];
}

function reinitialiserAffichage() {
  while (zoneMot.firstChild) {
    zoneMot.removeChild(zoneMot.firstChild);
  }
  for (let child of zoneClavier.childNodes) {
    child.disabled = false;
  }
  nombreErreurs.textContent = Number(0).toString();
  resultat.textContent = "";
}

function ajoutEcouteursTouches() {
  boutonNouvellePartie.addEventListener("click", async () => {await startGame()});
  for (let element of listeTouches) {
    element.addEventListener("click", async () => {
      element.disabled = true;
      await verifierLettre(motATrouver, element.innerText)

    })
  }
}

async function startGame() {
  count = 1;
  reinitialiserAffichage();
  changerImage();
  await choisirMot();
  nombreLettres = motATrouver.length;
  afficherEspacesMot(motATrouver);

}

function insererEspace() {
  let space = "_";
  let spaceElement = document.createElement("span");
  spaceElement.textContent = space;
  spaceElement.className = "espace-lettre";
  zoneMot.append(spaceElement);

}

function afficherEspacesMot(word) {
  for (let indexLettre = 0; indexLettre < word.length; indexLettre++) {
    insererEspace();

  }
}

function perdrePartie() {
  resultat.textContent = "Perdu! Le mot à trouver était : " + motATrouver;
  for (let childNode of zoneClavier.childNodes){
    childNode.disabled = true;
  }
}

async function avancerJeuMauvaiseReponse() {
  count += 1;
  changerImage();
  nombreErreurs.textContent = Number(count - 1).toString();
  if (count === 12) {
    changerImage();
    perdrePartie();


  }
}

function changerImage() {
  if (count <= 12) {
    imagePendu.setAttribute("src", "img/pendu" + count + ".jpg");
  } else {
    imagePendu.setAttribute("src", "img/pendu12.jpg");
  }
}

function setUpGame() {
  afficherClavier();
  ajoutEcouteursTouches();
}

setUpGame();
await startGame();
